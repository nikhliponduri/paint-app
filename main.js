import React from 'react'
import { View, Text } from 'react-native'
import { Provider } from 'react-redux'
import store from './src/store'
import App from './src/App'
import { userDetailsAction } from './src/actions/user.actions'
import { Root } from 'native-base'

store.dispatch(userDetailsAction())

const Main = () => {
    return (
        <Provider store={store}>
            <Root>
                <App />
            </Root>
        </Provider>
    )
}

export default Main
