import { combineReducers } from 'redux';
import adminReducer from './admin.reducer';
import userReducer from './user.reducer';
import sessionReducer from './session.reducer';
import commonReducer from './common.reducer';

export default combineReducers({
    admin: adminReducer,
    user: userReducer,
    session: sessionReducer,
    common: commonReducer
});