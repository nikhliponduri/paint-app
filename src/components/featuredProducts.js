import React, { useEffect } from 'react'
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native'
import { featuredProductsAction } from '../actions/user.actions';
import { connect } from 'react-redux';
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { getImageUrl } from '../utils/util';
import { withNavigation } from 'react-navigation';

const FeaturedProducts = (props) => {
    const { featuredProductsAction, featuredProducts, navigation: { navigate } } = props;
    useEffect(() => {
        featuredProductsAction();
    }, [featuredProductsAction]);
    if (!Array.isArray(featuredProducts)) return null;
    return (
        <View style={styles.Container}>
            <Text style={styles.Text}>Featured products</Text>
            <ScrollView horizontal={true}>
                {featuredProducts.map(product => {
                    return (
                        <TouchableWithoutFeedback onPress={() => navigate('Productdetail', { _id: product.data._id })} key={product._id}>
                            <Image style={styles.product} source={{ uri: getImageUrl(product.data.images[0]) }} />
                        </TouchableWithoutFeedback>
                    )
                })}
            </ScrollView>
        </View>
    )
}

const mapStateToProps = (state) => ({
    featuredProducts: state.common.featuredProducts
});

const mapDispatchToProps = {
    featuredProductsAction
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(FeaturedProducts));

const styles = StyleSheet.create({
    product: {
        width: Dimensions.get('window').width / 3,
        height: Dimensions.get('window').height / 6,
        margin: 5,
    },
    Container: {
        marginTop: 10
    },
    Text: {
        marginLeft: 5,
        marginBottom: 5,
        fontWeight: '400',
        fontSize: 20
    }
})