import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import { Dimensions, StyleSheet, View, Platform, ActivityIndicator } from 'react-native';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getPromoAction } from '../actions/user.actions';
import { getImageUrl } from '../utils/util';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
const { width: screenWidth } = Dimensions.get('window')

class MyCarousel extends Component {

    constructor(props) {
        super(props);
        this._renderItem = this._renderItem.bind(this)
    }
    componentDidMount() {
        this.props.getPromoAction();
    }

    _renderItem({ item}, parallaxProps) {
        const { navigation: { navigate } } = this.props
        return (
            <TouchableWithoutFeedback onPress={() => navigate('Productdetail', { _id: item.data._id })}>
                <View style={styles.item}>
                    <ParallaxImage
                        source={{ uri: getImageUrl(item.data.images[0]) }}
                        containerStyle={styles.imageContainer}
                        style={styles.image}
                        parallaxFactor={0.4}
                        {...parallaxProps}
                    />
                </View>
            </TouchableWithoutFeedback>
        );
    }

    render() {
        const { promos } = this.props;
        if (!promos) return <View style={{
            flex: 1,
            justifyContent: 'center',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 40
        }}>
            <ActivityIndicator size="large" color="#00ff00" />
        </View>
        return (
            <Carousel
                sliderWidth={screenWidth}
                sliderHeight={screenWidth}
                itemWidth={screenWidth - 60}
                data={promos}
                renderItem={this._renderItem}
                hasParallaxImages={true}
            />
        );
    }
}

const mapStateToProps = (state) => ({
    promos: state.common.promos
});

const mapDispatchToProps = {
    getPromoAction
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(MyCarousel));

const styles = StyleSheet.create({
    item: {
        width: screenWidth - 60,
        height: screenWidth - 60,
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderRadius: 8,
        marginTop: 5
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
    },
})