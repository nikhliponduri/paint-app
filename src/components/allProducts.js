import React, { useEffect } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { allProductsAction } from '../actions/user.actions';
import { connect } from 'react-redux';
import Product from './common/product';
import { ALL_PRODUCTS } from '../utils/constants';

const AllProducts = (props) => {
    const { allProductsAction, allProducts, loggedIn } = props;
    useEffect(() => {
        allProductsAction();
    }, [allProductsAction]);
    if (!Array.isArray(allProducts.data)) return null;
    return (
        <View>
            <Text style={styles.Text}>All Products</Text>
            <View style={styles.Container}>
                {allProducts.data.map((product, i) => <Product
                    loggedIn={loggedIn}
                    index={i}
                    key={product._id}
                    product={{ ...product }}
                    type={ALL_PRODUCTS}
                />)}
            </View>
        </View>
    )
}

const mapStateToProps = (state) => ({
    allProducts: state.common.allProducts,
    loggedIn: state.session.loggedIn
});

const mapDispatchToProps = {
    allProductsAction
}

export default connect(mapStateToProps, mapDispatchToProps)(AllProducts);

const styles = StyleSheet.create({
    Container: {
        marginTop: 20,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-evenly'
    },
    Text: {
        fontSize: 20,
        marginLeft: 5,
        marginTop: 20,
        fontWeight: '400'
    }
})