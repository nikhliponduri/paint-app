import React, { Component } from 'react';
import { Container, Spinner } from 'native-base';
export default class Loader extends Component {
  render() {
    return (
      <Container>
          <Spinner style={{alignItems:'center',justifyContent:'center'}} color='black' />
      </Container>
    );
  }
}