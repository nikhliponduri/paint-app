import React, { useState } from 'react';
import HeartIcon from 'react-native-vector-icons/Ionicons';
import { View, Image, StyleSheet, Dimensions } from 'react-native'
import { getImageUrl, resolveByType } from '../../utils/util';
import { likeProductAction, removeLikeAction, removeFromCartAction } from '../../actions/user.actions';
import { connect } from 'react-redux';
import { Toast, Footer, Spinner, Text } from 'native-base';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
import { LIKED_PRODUCTS } from '../../utils/constants';

const Product = (props) => {
    const { product: { images, liked, _id }, loggedIn, index, likeProductAction, removeLikeAction, navigation: { navigate }, removeFromCartAction, cart = false, type = 'ALL' } = props;
    const callback = ({ type, message }) => {
        resolveByType({
            type,
            failure: () => {
                Toast.show({ text: message, buttonText: 'Okay' });
            }
        })
    }
    const handleLike = () => {
        const productInfo = { productId: _id, type }
        liked || type === LIKED_PRODUCTS ? removeLikeAction(productInfo, callback) : likeProductAction(productInfo, callback);
    }
    const [cartLoading, setCartLoading] = useState(false);
    const handleCart = () => {
        setCartLoading(true);
        removeFromCartAction({ productId: _id }, callback)
    }
    return (
        <>
            <View style={[(index % 2 == 0) && styles.rightBorder, styles.border, styles.wrap]}>
                <TouchableWithoutFeedback onPress={() => navigate('Productdetail', { _id })}>
                    <Image style={styles.image} source={{ uri: getImageUrl(images[0]) }} />
                </TouchableWithoutFeedback>
                {loggedIn && !cart && <View style={styles.iconWrap}>
                    <HeartIcon onPress={handleLike} color={liked || type === LIKED_PRODUCTS ? 'red' : 'black'} size={20} style={styles.icon} name={'ios-heart'} />
                </View>}
                {cart &&
                    <Footer style={styles.footer}>
                        {cartLoading ? <Spinner /> : <TouchableWithoutFeedback onPress={handleCart}><Text style={styles.cart}>REMOVE FROM CART</Text></TouchableWithoutFeedback>}
                    </Footer>
                }
            </View>
        </>
    )
}

const mapDispatchToProps = {
    likeProductAction,
    removeLikeAction,
    removeFromCartAction
}

export default connect(null, mapDispatchToProps)(withNavigation(Product));

const styles = StyleSheet.create({
    image: {
        width: Dimensions.get('window').width / 2 - 20,
        height: Dimensions.get('window').height / 4,
    },
    border: {
        // borderBottomColor: 'black',
        // borderBottomWidth: 2,
    },
    rightBorder: {
        // borderRightColor: 'black',
        // borderRightWidth: 3
    },
    wrap: {
        position: 'relative',
        width: Dimensions.get('window').width / 2 - 20,
        // height: Dimensions.get('window').height / 4,
        backgroundColor: 'whitesmoke',
        marginTop: 3
    },
    icon: {
        margin: 'auto',
    },
    iconWrap: {
        position: 'absolute',
        top: 10,
        right: 10,
        width: 30,
        height: 30,
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50
    },
    footer: {
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
    },
    cart: {
        fontSize: 15,
        fontWeight: 'bold',
        color: 'white'
    }
})
