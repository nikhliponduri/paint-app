import { postUserLogin, getUserDetails, postUserRegister, getPromos, getLatestProducts, putProductLike, deleteProductLike, putIntoCart, deleteFromCart, getProductDetails, getCart, getUserLikedProducts, getAllProducts, getFeaturedProducts } from "../services/user.services";
import { setUserProfile, loginSucess, setSessionLoading, logoutSucess } from "../redux/session.reducer";
import axios from "axios";
import { AsyncStorage } from 'react-native';
import { checkError, sendCallback, PRODUCT_DETAIL, LATEST_PRODUCTS, LIKED_PRODUCTS, ALL_PRODUCTS } from "../utils/constants";
import { setPromos, setLatestProducts, setSelectedProduct, setProductLens, setAllProducts, setFeaturedProducts } from "../redux/common.reducer";
import { setUserCart, setUserLikedProducts } from "../redux/user.reducer";

export const userLoginAction = (info, callback) => async (dispatch) => {
    try {
        const res = await postUserLogin(info);
        await AsyncStorage.setItem('authtoken', `Bearer ${res.data.token}`);
        await dispatch(userDetailsAction(callback));
    } catch (error) {
        checkError(error, callback);
    }
}

export const userDetailsAction = (callback) => async (dispatch) => {
    try {
        axios.defaults.headers['Authorization'] = await AsyncStorage.getItem('authtoken');
        const res = await getUserDetails();
        dispatch(setUserProfile(res.data));
        dispatch(loginSucess());
        dispatch(setSessionLoading(false))
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback);
        dispatch(setSessionLoading(false))
    }
}

export const userRegisterAction = (info, callback) => async () => {
    try {
        await postUserRegister(info);
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback);
    }
}

export const userLogoutAction = (callback) => async (dispatch) => {
    await dispatch(logoutSucess())
    delete axios.defaults.headers.common["Authorization"];
    await AsyncStorage.removeItem('authtoken');
    dispatch(setSessionLoading(false));
    sendCallback(callback);
}

export const getPromoAction = (callback) => async (dispatch) => {
    try {
        const res = await getPromos();
        dispatch(setPromos(res.data));
        sendCallback(callback);
    } catch (error) {
        dispatch(setPromos(null));
        checkError(error, callback);
    }
}

export const latestProductsAction = (callback) => async (dispatch) => {
    try {
        const res = await getLatestProducts();
        dispatch(setLatestProducts(res.data));
        sendCallback(callback);
    } catch (error) {
        dispatch(setLatestProducts(null));
        checkError(error, callback);
    }
}

export const likeProductAction = (productInfo, callback) => async (dispatch) => {
    try {
        const { productId, type } = productInfo;
        await putProductLike(productId);
        dispatch(changeProductByType(type, productId, 'liked', true));
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback)
    }
}

export const removeLikeAction = (productInfo, callback) => async (dispatch, getState) => {
    try {
        const { user: { likedProducts } } = getState();
        const { productId, type } = productInfo;
        await deleteProductLike(productId);
        dispatch(changeProductByType(type, productId, 'liked', false));
        if (type === LIKED_PRODUCTS) {
            const index = likedProducts.findIndex(product => product.data._id === productId)
            if (index >= 0) {
                likedProducts.splice(index, 1);
                dispatch(setUserLikedProducts([...likedProducts]));
            }
            return
        }
        sendCallback(callback)
    } catch (error) {
        checkError(error, callback);
    }
}

export const addToCartAction = (productInfo, callback) => async (dispatch, getSatate) => {
    try {
        const { user: { cart } } = getSatate();
        const { productId, type } = productInfo;
        const res = await putIntoCart(productInfo.productId);
        cart && dispatch(setUserCart([res.data, ...cart]));
        dispatch(changeProductByType(type, productId, 'inCart', true));
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback);
    }
}

export const removeFromCartAction = (productInfo, callback) => async (dispatch, getState) => {
    try {
        const { productId, type } = productInfo;
        const { user: { cart } } = getState();
        await deleteFromCart(productId);
        if (Array.isArray(cart)) {
            const index = cart.findIndex(item => item.data._id === productId);
            if (index > -1) {
                cart.splice(index, 1);
                dispatch(setUserCart([...cart]));
            }
        }
        dispatch(changeProductByType(type, productId, 'inCart', false));
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback)
    }
}

export const getCartAction = (callback) => async (dispatch) => {
    try {
        const res = await getCart();
        dispatch(setUserCart(res.data));
        sendCallback(callback)
    } catch (error) {
        dispatch(setUserCart(null));
        checkError(error, callback);
    }
}

export const productDetailAction = (productInfo, callback) => async (dispatch) => {
    try {
        dispatch(setSelectedProduct(undefined));
        const res = await getProductDetails(productInfo.productId);
        dispatch(setSelectedProduct(res.data));
        dispatch(setProductLens(res.data));
        sendCallback(callback);
    } catch (error) {
        dispatch(setSelectedProduct(null));
        checkError(error, callback)
    }
}

export const changeProductByType = (type = 'ALL', productId, key, value) => (dispatch, getState) => {
    try {
        console.log(type, key, value)
        const { common: { selectedProduct, latestProducts, allProducts }, user: { likedProducts } } = getState();
        const updatedData = (products) => {
            if (!Array.isArray(products)) return false;
            const index = products.findIndex(product => product._id === productId)
            if (index < 0) return false;
            products[index][key] = value;
            return products
        }
        if (type === PRODUCT_DETAIL) {
            const newData = updatedData([selectedProduct]);
            if (newData) return dispatch(setSelectedProduct({ ...newData[0] }));
        }
        if (type === LATEST_PRODUCTS) {
            let newData = updatedData(latestProducts);
            if (newData) dispatch(setLatestProducts([...newData]));
            newData = updatedData(allProducts.data);
            if (newData) return dispatch(setAllProducts({
                ...allProducts,
                data: [...newData]
            }));
            return {};
        }
        if (type === LIKED_PRODUCTS) {
            const index = likedProducts.findIndex(product => product.data._id === productId)
            if (index >= 0) {
                likedProducts[index]['data'][key] = value;
                dispatch(setUserLikedProducts([...likedProducts]));
            }
            return {};
        }
        if (type === ALL_PRODUCTS) {
            let newData = updatedData(allProducts.data);
            if (newData) dispatch(setAllProducts({
                ...allProducts,
                data: newData
            }));
            newData = updatedData(latestProducts);
            if (newData) dispatch(setLatestProducts([...newData]));
            return {};
        }
        if (type === 'ALL') {
            if (Array.isArray(latestProducts)) {
                const newData = updatedData(latestProducts);
                if (newData) dispatch(setLatestProducts([...newData]));
            }
            if (Array.isArray(likedProducts)) {
                const index = likedProducts.findIndex(product => product.data._id === productId)
                if (index >= 0) {
                    likedProducts[index]['data'][key] = value;
                    dispatch(setUserLikedProducts([...likedProducts]));
                }
                return {};
            }
            return {};
        }
    } catch (error) {
        checkError(error);
        return {};
    }
}

export const userLikedProductsAction = (callback) => async (dispatch) => {
    try {
        const res = await getUserLikedProducts();
        dispatch(setUserLikedProducts(res.data));
        sendCallback(callback);
    } catch (error) {
        dispatch(setUserLikedProducts(null));
        checkError(error, callback);
    }
}

export const allProductsAction = (info = {}, callback) => async (dispatch, getState) => {
    const { common: { allProducts } } = getState();
    try {
        dispatch(setAllProducts({
            ...allProducts,
            data: undefined
        }));
        const { query } = info;
        const res = await getAllProducts(query);
        dispatch(setAllProducts(res.data));
        sendCallback(callback);
    } catch (error) {
        dispatch(setAllProducts({
            ...allProducts,
            data: null
        }))
        checkError(error, callback);
    }
}

export const featuredProductsAction = (callback) => async (dispatch) => {
    try {
        dispatch(setFeaturedProducts(undefined))
        const res = await getFeaturedProducts();
        dispatch(setFeaturedProducts(res.data));
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback);
    }
}