import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { productDetailAction, addToCartAction, removeFromCartAction } from '../actions/user.actions';
import { resolveByType, windowHeight, windowWidth, getImageUrl } from '../utils/util';
import { Toast, Footer, Spinner } from 'native-base';
import Loader from '../components/common/loader';
import { connect } from 'react-redux';
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { PRODUCT_DETAIL } from '../utils/constants';

const Productdetail = (props) => {
    const { navigation: { getParam }, productDetailAction, selectedProduct, addToCartAction, removeFromCartAction, loggedIn } = props;
    const [selectedImage, setSelectedImage] = useState('');
    const [loading, setloading] = useState(true);
    const [cartLoading, setCartLoading] = useState(false);
    useEffect(() => {
        productDetailAction({ productId: getParam('_id') }, ({ type, message }) => {
            setloading(false);
            resolveByType({
                type,
                failure: () => {
                    Toast.show({ text: message, buttonText: 'Okay' })
                }
            })
        });
    }, [productDetailAction, getParam, setloading]);
    if (!selectedProduct || loading) return <Loader />
    const { images, title, price, description, artist, _id, inCart } = selectedProduct;
    const handleCart = () => {
        const callback = ({ type, message }) => {
            setCartLoading(false);
            resolveByType({
                type,
                failure: () => {
                    Toast.show({ text: message, buttonText: 'Okay' });
                }
            })
        }
        setCartLoading(true);
        const productInfo = { productId: _id, type: PRODUCT_DETAIL }
        inCart ? removeFromCartAction(productInfo, callback) : addToCartAction(productInfo, callback)
    }
    return (
        <View style={styles.Container}>
            <ScrollView>
                <View style={styles.imageWrap}>
                    <Image style={styles.mainImage} source={{ uri: getImageUrl(selectedImage || selectedProduct.images[0]) }} />
                </View>
                {images.length > 1 &&
                    <ScrollView style={styles.subImageWrap} horizontal={true}>
                        {images.map((image, i) => <TouchableWithoutFeedback onPress={() => setSelectedImage(image)} key={i}><Image source={{ uri: getImageUrl(image) }} style={styles.subImage} /></TouchableWithoutFeedback>)}
                    </ScrollView>
                }
                <View style={styles.titleWrap}>
                    <Text style={styles.title}>{title}</Text>
                    <Text style={styles.price}>&#8377; {price}</Text>
                </View>
                <View style={styles.artistWrap}>
                    <Text style={styles.art1}>ART BY : </Text>
                    <Text style={styles.art2}> {artist}</Text>
                </View>
                <View style={styles.descriptionWrap}>
                    <Text style={styles.description}>{description}</Text>
                </View>
                <View style={{ marginTop: 20 }}></View>
                {loggedIn &&
                    <Footer style={styles.footer}>
                        {cartLoading ? <Spinner /> : <TouchableWithoutFeedback onPress={handleCart}><Text style={styles.cart}>{inCart ? 'REMOVE FROM CART' : 'ADD TO CART'}</Text></TouchableWithoutFeedback>}
                    </Footer>
                }
            </ScrollView>
        </View>
    )
}

const mapStateToProps = (state) => ({
    selectedProduct: state.common.selectedProduct,
    logedIn: state.session.loggedIn
});

const mapDispathToProps = {
    productDetailAction,
    addToCartAction,
    removeFromCartAction
}

export default connect(mapStateToProps, mapDispathToProps)(Productdetail);

const styles = StyleSheet.create({
    mainImage: {
        height: windowHeight / 1.5,
        width: windowWidth - 20,
    },
    imageWrap: {
        alignItems: 'center',
        justifyContent: "center",
        marginTop: 5
    },
    subImageWrap: {
        marginTop: 5,
        marginLeft: 10
    },
    subImage: {
        width: windowWidth / 3,
        height: windowHeight / 6,
        margin: 5,
    },
    Container: {
        position: 'relative',
        flex: 1
    },
    footer: {
        backgroundColor: '#7842E6',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleWrap: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
        marginLeft: 10,
        marginRight: 10
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20
    },
    price: {
        fontSize: 20
    },
    artistWrap: {
        flexDirection: 'row',
        margin: 10
    },
    art1: {
        fontWeight: 'bold',
        fontSize: 20
    },
    art2: {
        fontSize: 18
    },
    descriptionWrap: {
        marginTop: 20,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 20,
    },
    description: {
        fontSize: 16
    },
    cart: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    }
})