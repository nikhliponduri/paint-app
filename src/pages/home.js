/* eslint-disable react/display-name */
import React from 'react';
import { ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Carousal from '../components/carousal';
import FeaturedProducts from '../components/featuredProducts';
import AllProducts from '../components/allProducts';
import store from '../store';

const Home = () => {
    return (
        <ScrollView style={{ backgroundColor: 'white' }}>
            <Carousal />
            <FeaturedProducts />
            <AllProducts />
        </ScrollView>
    )
}

Home.navigationOptions = ({ navigation: { navigate },navigation }) => {
    const options = {
        title: 'Home',
        headerLeft: () => <Icon name='ios-menu' size={30} style={{ marginLeft: 15 }} backgroundColor="#000000" onPress={() => navigation.openDrawer()} />,
    }
    const { session: { loggedIn } } = store.getState();
    if (loggedIn) {
        options.headerRight = () => <Icon onPress={() => navigate('Cart')} name='ios-cart' style={{ marginRight: 20 }} size={30} />
    }
    return options
};

export default Home;

