/* eslint-disable react/display-name */
import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { getCartAction } from '../actions/user.actions';
import { connect } from 'react-redux';
import { resolveByType, windowHeight } from '../utils/util';
import Loader from '../components/common/loader';
import Product from '../components/common/product';
import { ScrollView } from 'react-native-gesture-handler';
import { Toast, Button } from 'native-base';

const Cart = (props) => {
    const { cart, getCartAction, navigation: { navigate } } = props;
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        getCartAction(({ type, message }) => {
            setLoading(false);
            resolveByType({
                type,
                failure: () => Toast.show({ text: message, buttonText: 'Okay' })
            })
        })
    }, [getCartAction, setLoading]);
    if (loading || cart === undefined) return <Loader />
    if (cart === null) return <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontSize: 16 }}>Something went wrong Please try again</Text></View>
    return (
        <ScrollView style={styles.Container}>
            <View style={styles.wrap}>
                {
                    !cart.length ?
                        <View style={styles.emptyWrap}>
                            <Text style={styles.empty}>Your Shopiing cart is empty</Text>
                            <Button onPress={() => navigate('Home')} style={styles.shoppingButton}><Text style={styles.shoppingText}>Continue Shopping</Text></Button>
                        </View> :
                        <View style={styles.cartWrap}>
                            {cart.map((product) => {
                                return (
                                    <Product
                                        key={product._id}
                                        product={product.data}
                                        cart
                                        loggedIn
                                    />
                                )
                            })
                            }
                        </View>
                }
            </View>
        </ScrollView>
    )
}

Cart.navigationOptions = ({ navigation: { openDrawer } }) => {
    const options = {
        title: 'Cart',
        headerLeft: () => <Icon name='ios-menu' size={30} style={{ marginLeft: 15 }} backgroundColor="#000000" onPress={() => openDrawer()} />,
    }
    return options
};

const mapStateToProps = (state) => ({
    cart: state.user.cart
});

const mapDispatchToProps = {
    getCartAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart)

const styles = StyleSheet.create({
    Container: {
        // marginTop: 10,
        marginLeft: 10,
    },
    Cart: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    empty: {
        fontSize: 18,
        fontWeight: 'normal'
    },
    emptyWrap: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        height: windowHeight / 2
    }, cartWrap: {
        marginTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        marginRight:10
    },
    shoppingButton: {
        backgroundColor: '#1890f2',
        marginTop: 20
    },
    shoppingText: {
        fontSize: 18,
        color: 'white',
        fontWeight: 'normal',
        padding: 15
    }
})