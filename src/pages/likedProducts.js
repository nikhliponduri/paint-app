/* eslint-disable react/display-name */
import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { getCartAction, userLikedProductsAction } from '../actions/user.actions';
import { connect } from 'react-redux';
import { resolveByType, windowHeight } from '../utils/util';
import Loader from '../components/common/loader';
import Product from '../components/common/product';
import { ScrollView } from 'react-native-gesture-handler';
import { Toast, Button } from 'native-base';
import { LIKED_PRODUCTS } from '../utils/constants';

const LikedProducts = (props) => {
    const { likedProducts, userLikedProductsAction, navigation: { navigate } } = props;
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        userLikedProductsAction(({ type, message }) => {
            setLoading(false);
            resolveByType({
                type,
                failure: () => Toast.show({ text: message, buttonText: 'Okay' })
            })
        })
    }, [getCartAction, setLoading]);
    if (loading || likedProducts === undefined) return <Loader />
    if (likedProducts === null) return <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontSize: 16 }}>Something went wrong Please try again</Text></View>
    return (
        <ScrollView style={styles.Container}>
            <View style={styles.wrap}>
                {
                    !likedProducts.length ?
                        <View style={styles.emptyWrap}>
                            <Text style={styles.empty}>Your wishlist is empty</Text>
                            <Button onPress={() => navigate('Home')} style={styles.shoppingButton}><Text style={styles.shoppingText}>Continue Shopping</Text></Button>
                        </View> :
                        <View style={styles.cartWrap}>
                            {likedProducts.map((product) => {
                                return (
                                    <Product
                                        key={product._id}
                                        product={product.data}
                                        type={LIKED_PRODUCTS}
                                        loggedIn
                                    />
                                )
                            })
                            }
                        </View>
                }
            </View>
        </ScrollView>
    )
}


const mapStateToProps = (state) => ({
    likedProducts: state.user.likedProducts
});

const mapDispatchToProps = {
    userLikedProductsAction
}

export default connect(mapStateToProps, mapDispatchToProps)(LikedProducts)

const styles = StyleSheet.create({
    Container: {
        // marginTop: 10,
        marginLeft: 10,
    },
    Cart: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    empty: {
        fontSize: 18,
        fontWeight: 'normal'
    },
    emptyWrap: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        height: windowHeight / 2
    }, cartWrap: {
        marginTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        marginRight: 10
    },
    shoppingButton: {
        backgroundColor: '#1890f2',
        marginTop: 20
    },
    shoppingText: {
        fontSize: 18,
        color: 'white',
        fontWeight: 'normal',
        padding: 15
    }
})