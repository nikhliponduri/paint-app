import React, { useState } from 'react';
import { withNavigation } from 'react-navigation';
import { Text, View, StyleSheet, Dimensions, ScrollView, Button } from 'react-native';
import { Container, Input, Toast } from 'native-base';
import UserIcon from 'react-native-vector-icons/AntDesign';
import { connect } from 'react-redux';
import { userRegisterAction } from '../actions/user.actions';
import { isValidLoginForm } from '../validation/validations';
import { resolveByType } from '../utils/util';

const Register = (props) => {
    const { userRegisterAction, navigation:{navigate} } = props;
    const [loginInfo, setLoginInfo] = useState({ email: '', password: '' });
    const [formErrors, setFormErrors] = useState({});

    const handleLogin = () => {
        setFormErrors({});
        const { email, password } = loginInfo;
        const isValidForm = isValidLoginForm({ email, password });
        if (!isValidForm.isValidForm) {
            setFormErrors(isValidForm.errors);
            return;
        }
        const callback = ({ type, message }) => {
            resolveByType({
                type,
                success: () => {
                    navigate('Login');
                    Toast.show({text:'Login with your credentials',buttonText:'Okay'})
                },
                failure: () => {
                    Toast.show({
                        text:message,
                        buttonText:'Okay'
                    })
                }
            })
        }
        userRegisterAction({ email, password }, callback);
    }

    return (
        <ScrollView style={{ backgroundColor: '#FFFFFF' }}>
            <Container style={styles.Container}>
                <Text style={styles.t1}>
                    Proceed with your
            </Text>
                <Text style={styles.t2}>Registration</Text>
                <Container>
                    <View style={styles.inputWrap}>
                        <Text style={styles.inputText}>Email</Text>
                        <View style={styles.inputWrap2}>
                            <Input onChangeText={(email) => setLoginInfo({ ...loginInfo, email })} style={styles.input} value={loginInfo.email} />
                            <UserIcon style={styles.icon} name='user' size={25} />
                        </View>
                        {formErrors.email && <Text style={[styles.error]}>{formErrors.email}</Text>}
                    </View>
                    <View style={styles.inputWrap}>
                        <Text style={styles.inputText}>Password</Text>
                        <View style={styles.inputWrap2}>
                            <Input secureTextEntry={true} onChangeText={(password) => setLoginInfo({ ...loginInfo, password })} style={styles.input} value={loginInfo.password} />
                            <UserIcon style={styles.icon} name='key' size={25} />
                        </View>
                        {formErrors.password && <Text style={[styles.error]}>{formErrors.password}</Text>}
                    </View>
                    <View style={{ marginTop: 30 }}>
                        <Button onPress={handleLogin} style={{ marginTop: 40 }} title='Login' />
                    </View>
                </Container>
            </Container>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    Container: {
        paddingLeft: 40,
        marginRight: 20
    },
    t1: {
        color: '#252525',
        fontSize: 20,
        marginTop: Dimensions.get('window').height / 6
    },
    t2: {
        fontSize: 30,
        fontWeight: '700'
    },
    inputWrap: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginTop: 30
    },
    input: {
        marginBottom: 0,
        paddingBottom: 0
    },
    inputWrap2: {
        flexDirection: 'row',
        borderBottomColor: '#C5C5C5',
        borderBottomWidth: 2,
        alignItems: 'baseline',
    },
    icon: {
        paddingBottom: 5
    },
    inputText: {
        fontSize: 15,
        fontWeight: 'bold',
        marginLeft: 5
    },
    submit: {
        backgroundColor: 'red',
        textAlignVertical: 'center',
        textAlign: 'center',
        color: 'white',

    },
    error: {
        color: 'red',
        marginTop: 0
    }
});

const mapDispatchToProps = {
    userRegisterAction
}
export default connect(null, mapDispatchToProps)(withNavigation(Register));
