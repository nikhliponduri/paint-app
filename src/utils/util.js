import { SUCCESS, FAILURE, S3_URL } from "./constants";
import { Dimensions } from "react-native";

export const resolveByType = ({ type, success, failure }) => {
    if (type === SUCCESS) {
        success && success();
    } else if (type === FAILURE) {
        failure && failure();
    }
}

export const checkLoading = (loadingInfo) => {
    if (loadingInfo === null) return null;
    if (!loadingInfo) return true;
    return false;
}

export const getDiscountPercentage = (price, discount) => {
    return parseInt((discount / price) * 100)
}

export const getImageUrl = (url) => {
    return `${S3_URL}/${url}`;
}

export const windowLocation = () => window.location.href;

export const clickLogin = () => {
    const elem = document.getElementById('login-box');
    if (elem) elem.click();
}

export const windowHeight = Dimensions.get('window').height

export const windowWidth = Dimensions.get('window').width