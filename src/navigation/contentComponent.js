import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import SimpleLinneIcon from 'react-native-vector-icons/SimpleLineIcons';
import EmailIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import { View, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { Container } from 'native-base';
import { userLogoutAction } from '../actions/user.actions';
import store from '../store';


const getActiveRouteState = function (route) {
    if (!route.routes || route.routes.length === 0 || route.index >= route.routes.length) {
        return route;
    }

    const childActiveRoute = route.routes[route.index];
    return getActiveRouteState(childActiveRoute);
}


const contentComponent = (props) => {
    const { navigation: { state, navigate }, loggedIn } = props;
    const isRouteMatches = (route) => getActiveRouteState(state).routeName === route
    return (
        <View style={styles.fullBackground}>
            <Container style={styles.container}>
                {
                    loggedIn &&
                    <View style={styles.userIcon}>
                        <EvilIcon name='user' size={120} />
                    </View>
                }
                <TouchableWithoutFeedback onPress={() => navigate('Home')}>
                    <View style={[styles.navigations, isRouteMatches('Home') ? styles.active : {}]} >
                        <Icon style={[styles.navigationIcon, isRouteMatches('Home') ? styles.active : {}]} size={27} name='ios-home' />
                        <Text style={[styles.navigationText, { marginLeft: 23 }, isRouteMatches('Home') ? styles.active : {}]}>Home</Text>
                    </View>
                </TouchableWithoutFeedback>
                {!loggedIn ?
                    <>
                        <TouchableWithoutFeedback onPress={() => navigate('Register')}>
                            <View style={[styles.navigations, isRouteMatches('Register') ? styles.active : {}]} >
                                <EmailIcon style={[styles.navigationIcon, isRouteMatches('Register') ? styles.active : {}]} size={25} name='email' />
                                <Text style={[styles.navigationText, isRouteMatches('Register') ? styles.active : {}]}>Register</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => navigate('Login')}>
                            <View style={[styles.navigations, isRouteMatches('Login') ? styles.active : {}]} >
                                <SimpleLinneIcon style={[styles.navigationIcon, isRouteMatches('Login') ? styles.active : {}]} size={25} name='login' />
                                <Text style={[styles.navigationText, isRouteMatches('Login') ? styles.active : {}]}>Login</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </>
                    : <>
                        <TouchableWithoutFeedback onPress={() => navigate('Likedproducts')}>
                            <View style={[styles.navigations, isRouteMatches('Likedproducts') ? styles.active : {}]}>
                                <Icon style={[styles.navigationIcon, isRouteMatches('Likedproducts') ? styles.active : {}, { marginTop: 5 }]} size={25} name='ios-heart' />
                                <Text style={[styles.navigationText, isRouteMatches('Likedproducts') ? styles.active : {}]}>Liked Products</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => navigate('Cart')}>
                            <View style={[styles.navigations, isRouteMatches('Cart') ? styles.active : {}]}>
                                <EmailIcon style={[styles.navigationIcon, isRouteMatches('Cart') ? styles.active : {}]} size={25} name='cart' />
                                <Text style={[styles.navigationText, isRouteMatches('Cart') ? styles.active : {}]}>Cart</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => {
                            store.dispatch(userLogoutAction());
                            navigate('Login');
                        }}>
                            <View style={styles.navigations}>
                                <EmailIcon style={styles.navigationIcon} size={25} name='logout' />
                                <Text style={styles.navigationText}>Logout</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </>
                }
            </Container>
        </View>
    )
}

const styles = StyleSheet.create({
    fullBackground: {
        flex: 1,
        backgroundColor: 'white'
        // height: Dimensions.get('window').height,
    },
    navigations: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        paddingLeft: 20,
        alignItems: 'center',
        height: 40,
        marginTop: 10
    },
    navigationText: {
        fontSize: 20,
        marginLeft: 20,
        paddingTop: 3
    },
    navigationIcon: {

    },
    container: {
        marginTop: 30,
    },
    active: {
        backgroundColor: 'whitesmoke',
        color: '#4FA9F3'
    },
    userIcon: {
        alignItems: 'center',
        marginBottom: 20
    }
})

export default contentComponent;
