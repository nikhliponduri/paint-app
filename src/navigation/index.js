import React from 'react';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import Login from '../pages/login';
import Register from '../pages/register';
import contentComponent from './contentComponent';
import Home from '../pages/home';
import { Button } from 'native-base';
import Productdetail from '../pages/productDetail';
import Cart from '../pages/cart';
import LikedProducts from '../pages/likedProducts';

export default ({ loggedIn }) => {
    let options = {
        Home: createStackNavigator({
            Home,
            Productdetail: Productdetail
        }, {
            initialRouteName: 'Home'
        }),
    }
    if (!loggedIn) {
        options = {
            ...options,
            Login,
            Register
        }
    }
    else {
        options = {
            ...options,
            Cart: createStackNavigator({
                Cart: Cart
            }),
            Likedproducts: LikedProducts
        }
    }
    return createDrawerNavigator(options, {
        contentComponent: (props) => contentComponent({ ...props, loggedIn }),
        initialRouteName: 'Home',
        minSwipeDistance: 1,
        navigationOptions: {

        },
        defaultNavigationOptions: ({ navigation }) => {
            return {
                // eslint-disable-next-line react/display-name
                headerLeft: () => <Button title='Info' color="#222" onPress={() => navigation.toggleDrawer()} />

            }
        }
    });
}