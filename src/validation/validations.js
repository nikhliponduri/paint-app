import validator, { TEXT, EMAIL, PASSWORD, NUMERIC } from "./validator";

export const isValidRegisterForm = (info) => {
    const { email = '', password = '' } = info;
    const options = [
        { type: EMAIL, value: email, key: 'email', min: 1, max: 40 },
        { type: PASSWORD, value: password, key: 'password', message: 'Password should be between 6 and 20 alphanumeric characters' },
    ]
    const isValidForm = validator(options);
    return isValidForm;
}

export const isValidLoginForm = ({ email = '', password = '' }) => {
    const options = [
        { type: EMAIL, value: email, key: 'email', min: 1 },
        { type: PASSWORD, value: password, min: 1, max: 20, key: 'password' },
    ]
    return validator(options);
}

export const isValidAddProductform = ({ title, description, price, artist, discount }) => {
    const options = [
        { type: TEXT, value: title, min: 1, max: 20, key: 'title' },
        { type: TEXT, value: description, min: 1, max: 1000, key: 'description' },
        { type: NUMERIC, value: price, key: 'price' },
        { type: TEXT, value: artist, key: 'artist', min: 1, max: 20 }
    ]
    const isValidForm = validator(options);
    if (!isValidForm.errors.price) {
        if (price <= 0) {
            isValidForm.isValidForm = false;
            isValidForm.errors.price = 'Please enter a valid price';
        }
    }
    if (!isNaN(price) && discount && discount >= price) {
        isValidForm.isValidForm = false;
        isValidForm.errors.discount = 'Please enter a valid discount';
    }
    return isValidForm;
}