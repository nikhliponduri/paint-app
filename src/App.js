import React from 'react'
import { connect } from 'react-redux';
import { createAppContainer } from 'react-navigation';
import Drawer from './navigation'

const App = ({ loggedIn }) => {
  const Navigation = createAppContainer(Drawer({ loggedIn }));
  return <Navigation />
}

const mapStateToProps = (state) => ({
  loggedIn: state.session.loggedIn
})

export default connect(mapStateToProps)(App);
