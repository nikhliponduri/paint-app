import Axios from "axios";
import { API, makeRequest, POST, PUT, DELETE } from "../utils/fetch";

export const postAddProduct = (body) => {
    return Axios.post(`${API}/products`, body, {
        headers: {
            'content-type': 'multipart/form-data'
        }
    })
}

export const postPromo = (body) => {
    return makeRequest(POST, '/admin/promo', body);
}

export const putFeatureItem = (productId) => {
    return makeRequest(PUT, `/products/${productId}/feature`);
}

export const deleteFeatureItem = (productId) => {
    return makeRequest(DELETE, `/products/${productId}/feature`);
}