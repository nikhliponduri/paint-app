import { makeRequest, GET, POST, PUT, DELETE } from "../utils/fetch";

export const postUserLogin = (body) => {
    return makeRequest(POST, '/user/login', body);
}

export const getUserDetails = (body) => {
    return makeRequest(GET, '/user/ping');
}

export const postUserRegister = (body) => {
    return makeRequest(POST, `/user/register`, body);
}

export const getPromos = () => {
    return makeRequest(GET, '/admin/promo');
}

export const getLatestProducts = () => {
    return makeRequest(GET, '/products/latest');
}

export const putProductLike = (productId) => {
    return makeRequest(PUT, `/products/${productId}/like`);
}

export const deleteProductLike = (productId) => {
    return makeRequest(DELETE, `/products/${productId}/like`);
}

export const putIntoCart = (productId) => {
    return makeRequest(PUT, `/cart/${productId}`);
}

export const deleteFromCart = (productId) => {
    return makeRequest(DELETE, `/cart/${productId}`);
}

export const getProductDetails = (productId) => {
    return makeRequest(GET, `/products/${productId}`);
}

export const getCart = () => {
    return makeRequest(GET, `/cart`);
}

export const getUserLikedProducts = () => {
    return makeRequest(GET, `/products/liked`);
}

export const getAllProducts = (query) => {
    return makeRequest(GET, `/products/all/?${query}`);
}

export const getFeaturedProducts = () => {
    return makeRequest(GET, '/products/featured');
}