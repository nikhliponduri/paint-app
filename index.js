/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Main from './main';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';

// eslint-disable-next-line no-undef
GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;


AppRegistry.registerComponent(appName, () => gestureHandlerRootHOC(Main));
